# Desafio Android - O POVO #
Criar um aplicativo para consultar a [API do OpenWeatherMap](http://openweathermap.org/api) e trazer as previsões de tempo. Seguem os mockups para se basear:

![Screenshot_20160318-152432.png](https://bitbucket.org/repo/kdeM4n/images/2455605288-Screenshot_20160318-152432.png)
![Screenshot_20160318-152438.png](https://bitbucket.org/repo/kdeM4n/images/2338022819-Screenshot_20160318-152438.png)
![Screenshot_20160318-152445.png](https://bitbucket.org/repo/kdeM4n/images/3628779820-Screenshot_20160318-152445.png)

## Deve conter ##
* Previsões do tempo de 5 dias da geolocalização do usuário.
* Ao tocar em um item, deve levar para os detalhes da previsão.
* Configurações com opção de alterar para Celsius ou Fahrenheit.
* Caso o usuário estiver sem conexão, deve exibir dados persistidos do último acesso com conexão.
## Deve conter ##
* Arquivo .gitignore
* Framework para Comunicação com API. Ex: Retrofit
* Mapeamento json -> Objeto . Ex: Gson
* Cache de Imagens. Ex Picasso
## Sugestões ##
As sugestões de bibliotecas fornecidas são só um guideline, sintam-se a vontade para usar diferentes e nos surpreenderem. O importante de fato é que os objetivos macros sejam atingidos. =)
## OBS ##
A foto do mockup é meramente ilustrativa.
## Processo de submissão ##
O candidato deverá implementar a solução e enviar um pull request para este repositório com a solução.

O processo de Pull Request funciona da seguinte maneira:
1. Candidato fará um fork desse repositório (não irá clonar direto!)
1. Fará seu projeto nesse fork.
1. Commitará e subirá as alterações para o SEU fork.
1. Pela interface do Bitbucket, irá enviar um Pull Request.
Se possível deixar o fork público para facilitar a inspeção do código.
## ATENÇÃO ##
Não se deve tentar fazer o PUSH diretamente para ESTE repositório!